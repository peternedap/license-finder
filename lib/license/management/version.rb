# frozen_string_literal: true

module License
  module Management
    VERSION = '3.29.1'
  end
end
