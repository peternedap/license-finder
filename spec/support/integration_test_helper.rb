# frozen_string_literal: true

module IntegrationTestHelper
  def runner(*args)
    @runner ||= ProjectHelper.new(*args)
  end
end
